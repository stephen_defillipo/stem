	translate([0,0,20])
linear_extrude(h=10,twist=100)
	minkowski()
	{
		circle(10);
		for(i=[0:10])
			{
			rotate([1,1,360/10 * i])
				hull()
				{
					translate([10,0,0]) circle(1);
					circle(1);
				}
			}
	}
	rotate([0,0,20])
linear_extrude(height=20,twist=20)
	minkowski()
	{
		circle(10);
		for(i=[0:10])
			{
			rotate([1,1,360/10 * i])
				hull()
				{
					translate([10,0,0]) circle(1);
					circle(1);
				}
			}
	}
